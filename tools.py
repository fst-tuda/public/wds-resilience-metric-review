def perc(i, len_df):
    return round(100 * i / len_df, 2)


def delete_linebreak(s):
    return s.replace("\n", "")


def add_citation(row, metric_row_name="metric", citation_row_name="citation"):
    s = "{} \cite{{{}}}".format(row[metric_row_name], row[citation_row_name])
    return s
