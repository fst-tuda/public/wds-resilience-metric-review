# WDS Resilience Metric Review

## Description
This project is a research object for the publication "Do Resilience Metrics of Water Distribution Systems Really Assess Resilience? A Critical Review" by the following authors:
- Michaela Leštáková, orcid: 0000-0002-5998-6754 (Corresponding author: michaela.lestakova@fst.tu-darmstadt.de)
- Kevin T. Logan, orcid: 0000-0001-5512-2679
- Imke Rehm, orcid: 0000-0001-9751-3934
- John Friesen, orcid: 0000-0003-2530-1363
- Peter F. Pelz, orcid: 0000-0002-0195-627X

The Review studies metrics for assessing the resilience of water distribution systems.
The metrics found in the literature search are categorised using a novel framework according to various characteristics of the metrics as well as according to which properties/functions of resilient systems they assess.

This GitLab repository contains Jupyter notebooks that use the original datasets used to generate the results presented in the Review. The datasets are stored permanently in the institutional data repository of TU Darmstadt, [TUdatalib](https://tudatalib.ulb.tu-darmstadt.de/). The notebooks recreate the whole analysis and all diagrams presented in the Review.

The commit hash for the version of the repository as it was upon submission is fa5843c3217468a657dc5ec92c6c3e27241f1478.
It is also available as a ZIP file on TUdatalib: https://tudatalib.ulb.tu-darmstadt.de/handle/tudatalib/3901

## Installation

### Clone this repository
Access the folder/directory of your choice and clone this repository on your machine by running

```bash
git clone git@git.rwth-aachen.de:fst-tuda/public/wds-resilience-metric-review.git
```

### Create and activate the virtual environment

Create the virtual environment using conda by running

```bash
conda env create -f environment.yml
```
Activate the virtual environment with

```bash
conda activate metrics-review
```

## Usage
Open and run the Jupyter notebooks in an editor of your choice (such as Visual Studio Code).

After opening the Jupyter notebooks, do not forget to select the right kernel (`metrics-review`).

You can also use the web interface for Jupyter notebooks, accessible upon executing

```bash
jupyter notebook
```
in the base of this repository from the terminal.

To check out the commit hash for the version of the repository as it was upon submission, run

```bash
git checkout fa5843c3217468a657dc5ec92c6c3e27241f1478
```

## Support
Questions can be submitted as issues in this repository or e-mailed to michaela.lestakova@fst.tu-darmstadt.de


## Authors and acknowledgment

* Michaela Le\v{s}táková
* Kevin Logan
* Imke-Sophie Rehm
* John Friesen

This work has been funded by the LOEWE initiative (Hesse, Germany) within the emergenCITY center and the LOEWE exploration project "Uniform detection and modeling of slums to determine infrastructure needs", as well as by the KSB Stiftung Stuttgart, Germany within the project “Antizipation von Wasserbedarfsszenarien für die Städte der Zukunft”.


## License
MIT License

Copyright (c) 2023 Michaela Leštáková, Kevin Logan, Imke-Sophie Rehm, John Friesen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Project status
Final version